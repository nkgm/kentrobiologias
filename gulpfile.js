var gulp = require('gulp');

var browserSync = require('browser-sync').create();
var reload = browserSync.reload;
var sass = require('gulp-sass');
var panini = require('panini');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var cache = require('gulp-cache');
var imagemin = require('gulp-imagemin');
var concat = require('gulp-concat');
var gutil = require('gulp-util');
var ftp = require('vinyl-ftp');

function ftp_conn() {
  return ftp.create({
    host: process.env.FTP_HOST,
    user: process.env.FTP_USER,
    password: process.env.FTP_PASS,
    parallel: 4,
    log: gutil.log
  });
}

gulp.task('deploy', function() {
  var conn = ftp_conn();
  return gulp.src('dist/**', { base: 'dist', buffer: false })
    .pipe(conn.newer('/httpdocs'))
    .pipe(conn.dest('/httpdocs'));
});

gulp.task('ftp-clean', function(cb) {
  return ftp_conn().rmdir('/httpdocs', function(err) {
    cb(err);
  });
});

gulp.task('sass', function() {
  return gulp.src('src/assets/scss/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer('last 2 versions'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('dist/assets/css'))
    .pipe(browserSync.stream());
  console.log('Compiling scss');
});

gulp.task('compile-html', function() {
  return gulp.src('src/pages/**/*.html')
    .pipe(panini({
      root: 'src/pages/',
      layouts: 'src/layouts/',
      partials: 'src/partials/',
      helpers: 'src/helpers/',
      data: 'src/data/'
    }))
    .pipe(gulp.dest('dist'));
  console.log('Compiling partials with Panini');
});

gulp.task('scripts-vendor', function () {
  // jQuery first, then Popper.js, then Bootstrap JS, then other JS libraries, and last app.js
  return gulp.src([
    'node_modules/jquery/dist/jquery.js',
    'node_modules/popperjs/dist/umd/popper.js',
    'node_modules/bootstrap/dist/js/bootstrap.js'
  ])
    .pipe(sourcemaps.init())
    .pipe(concat('vendor.js'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('dist/assets/js/'))
    .pipe(browserSync.stream());
  console.log('Concatenating JavaScript files into single file');
});

gulp.task('reset-pages', (done) => {
  panini.refresh();
  done();
  console.log('Clearing panini cache');
});

gulp.task('images', function () {
  return gulp.src('src/assets/img/**/*.+(png|jpg|jpeg|gif|svg)')
    .pipe(cache(imagemin ())) // Caching images that ran through imagemin
    .pipe(gulp.dest('dist/assets/img/'));
  console.log('Optimizing images');
});

gulp.task('serve', ['compile-html', 'sass', 'images', 'scripts-vendor'], function() {
  // Live reload with BrowserSync
  browserSync.init({
    server: "./dist",
    notify: false,
    open: false
  });

  gulp.watch(['src/assets/js/partials/*.js'], ['scripts', browserSync.reload]);
  gulp.watch(['node_modules/bootstrap/scss/bootstrap.scss', 'src/assets/scss/**/*'], ['sass', browserSync.reload]);
  gulp.watch(['src/assets/img/**/*'], ['images']);
  gulp.watch(['src/assets/vid/**/*'], ['media']);
  gulp.watch(['src/**/*.html'], ['reset-pages', 'compile-html', browserSync.reload]);
  console.log('Watching for changes');
});

gulp.task('default', ['serve']);
